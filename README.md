# RC_RBP

Final files for the project 'Through segments,' the installation at Kunsthaus, Graz

# Supercollider
version 3.10.4

Plugin version 3.10

# Audio setting
PCM capture source: LINE

See audioSetting1.png, audioSetting2.png for the detail.

# To start the piece
Put all the .scd files in one folder.
The default path is /home/pi/Desktop
In the case of using a different folder, please change the path in the loding.scd file. (line 12)

### using command line:
sclang /home/pi/foldername/loading.scd &

### using SuperCollider IDE:
execute loading.scd file.

### autostart
in order to start up the piece automatically after the boot, put autostart.sh file to ~/.config/autostart/

Make sure to change the path for the SuperCollider patches, where it is set to 'Desktop' folder.  

echo " " | sclang /home/pi/Desktop/loading.scd > /home/pi/Desktop/sclang.log & 


# GUI
It is not necessary to play with the GUI, but in the case of the output level too loud, then the last slider 'amp' works as a global level, not in dB, but in Amp(0~1). It is by default set to the maximum value 1. Notice that the change of the Amp value takes 7 seconds.

# Network address from other users
In the case of changing the network address, the address is written in the middle of loading.scd file.
