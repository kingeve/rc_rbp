var counter=0;
OSCdef(\monitor,{|msg, time|

	if(msg[3] == 8){
		//quiet
		msg[3].postln;
		~addrHanns.sendMsg(\active, "ji",0);
		~addrDanielle.sendMsg(\active, "ji",0);
		~addrDavid.sendMsg(\active, "ji",0);
		OSCdef(\monitor).disable;
		OSCdef(\monitor2).enable;
	};
},'/tr');
OSCdef(\monitor2,{|msg|
	if(msg[3] == 9){
		msg[3].postln;
		~addrHanns.sendMsg(\active, "ji",1);
		~addrDanielle.sendMsg(\active, "ji",1);
		~addrDavid.sendMsg(\active, "ji",1);
		OSCdef(\monitor).enable;
		OSCdef(\monitor2).disable;
	};
},'\tr');
OSCdef(\monitor).enable;
//////////////////////////////////////
OSCdef(\organ,{|msg, time|

	if (msg[3] == 1.0){
		var duration = 30.0, which, speed;
		duration = msg[2]*1.0.rand;
		speed = duration * 0.001.rand;
		which = 4.rand + 1;
		("ORGAN:" + duration + "speed:" + speed +"which:" + which).postln;OSCdef(\organ).disable;
		Routine.new({
			~spat.set(\which, which,\speed, speed);0.01.wait;
			~noisesynth.set(\gate,0);
			0.01.wait;
			~normal2.set(\amp,0);
			0.01.wait;
			~base.set(\amp,0.008);
			0.01.wait;
			~normal1.set(\inbus,10,\outbus2,12,\amp,0.0);
			0.01.wait;
			~organ.set(\inbus,10,\gate,1,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.32, \timeDev,1.0.rand,\psfb,0.9);
			(duration*0.5).wait;
			~fineGrain.set(\gate,1,\ps, 1.0.rand);
			(duration*0.1).wait;
			~organ.set(\inbus,10,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.32, \timeDev,1.0.rand,\psfb,0.9);(duration*0.2).wait;

			~organ.set(\inbus,10,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.32, \timeDev,1.0.rand,\psfb,0.9);(duration*0.2).wait;

			~organ.set(\gate,0,\inbus,12,\pitchDev,0,\amp,0.32, \tr,0.2,\timeDev,0.004,\psfb,0.5);0.01.wait;
			~fineGrain.set(\gate,0);0.01.wait;
			~normal1.set(\amp,0.3);0.01.wait;
			~noisesynth.set(\gate,1);0.01.wait;
			~normal2.set(\amp,0.3);0.01.wait;
			"here I am".postln;0.01.wait;
			5.0.wait;
			~base.set(\amp,0.08);
			OSCdef(\organ).enable;0.01.wait;
			// ~remoteAddr.sendMsg(\remoteSend, 10);
		}).play;
	};

	if (msg[3] == 2.0){
		var duration = 30.0, which, speed;
		duration = msg[2]*1.0.rand;
		speed = duration * 0.001.rand;
		which = 4.rand + 1;
		("ATTACK:" + duration + "speed:" + speed+"which:" + which).postln;OSCdef(\organ).disable;


		Routine.new({
			~organ.set(\gate,0);
			0.01.wait;
			~fineGrain.set(\gate,0);
			0.01.wait;
			~input1.set(\inputLevel, 1.0);
			0.01.wait;
			~af.set(\gate,1,\inbus,10,\outbus2,12,\boost, 3.0,\tr,0.3);
			0.01.wait;
			~spat.set(\which, which,\speed, speed);
			0.01.wait;
			~base.set(\amp,0.05);
			0.01.wait;
			~noiseSynth.set(\gate,0);
			0.01.wait;
			~normal2.set(\gate,0);
			0.01.wait;
			~normal1.set(\inbus,12,\outbus2,14,\amp,0.7);
			0.01.wait;

			~harm.set(\gate,1,\inbus,14);
			~organ.set(\gate,1,\inbus,14,\outbus2,26,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.4, \timeDev,1.0.rand);
			duration.wait;
			"here I am".postln;

			~input1.set(\inputLevel, 1.0);0.01.wait;
			~base.set(\amp,0.08);0.01.wait;
			~af.set(\gate,0);2.0.wait;
			~harm.set(\gate,0);2.0.wait;
			~normal1.set(\inbus,10,\outbus2,12,\amp,0.6);0.01.wait;
			~noiseSynth.set(\gate,1);0.01.wait;
			~normal2.set(\gate,1);0.01.wait;
			~organ.set(\gate,1,\inbus, 12,\outbus2, 14,\amp,0.4);0.01.wait;
			5.0.wait;
			OSCdef(\organ).enable;0.01.wait;
			// ~remoteAddr.sendMsg(\remoteSend, 10);
		}).play;
	};
	if (msg[3] == 3.0){
		var duration = 30.0, which, speed;
		duration = msg[2]*1.0.rand;
		speed = duration * 0.001.rand;
		which = 4.rand + 1;
		("Fine:" + duration + "speed:" + speed+"which:" + which).postln;OSCdef(\organ).disable;

		Routine.new({
			~input1.set(\inputLevel, 1.0);
			0.01.wait;
			~spat.set(\which, which,\speed, speed);
			0.01.wait;
			~base.set(\amp,0.008);
			0.01.wait;
			~normal1.set(\inbus,10,\outbus2,12,\amp,0.6);
			(duration*0.2).wait;
			~organ.set(\inbus,12,\gate,1,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.3, \timeDev,1.0.rand);
			(duration*0.2).wait;
			~fineGrain.set(\gate,1,\ps, 1.0.rand);
			(duration*0.2).wait;
			~noiseSynth.set(\amp,0.8);
			(duration*0.2).wait;
			~normal2.set(\amp,0.5);
			(duration*0.2).wait;
			"here I am".postln;

			~organ.set(\gate,0);0.01.wait;
			~fineGrain.set(\gate,0);5.0.wait;
			OSCdef(\organ).enable;0.01.wait;
			// ~remoteAddr.sendMsg(\remoteSend, 10);
		}).play;
	};
	if (msg[3] == 4.0){
		var duration = 30.0, which, speed;
		duration = msg[2]*1.0.rand;
		speed = duration * 0.001.rand;
		which = 4.rand + 1;
		("Amp:" + duration + "speed:" + speed+"which:" + which).postln;OSCdef(\organ).disable;

		Routine.new({
			~input1.set(\inputLevel, 0.7);
			0.01.wait;
			~spat.set(\which, which,\speed, speed);
			0.01.wait;
			~normal1.set(\gate,0);
			0.01.wait;
			~normal2.set(\gate,0);
			0.01.wait;
			~organ.set(\gate,0);
			0.01.wait;
			~base.set(\gate,0);
			0.01.wait;
			~noiseSynth.set(\gate,1,\amp,0.7);
			(duration*0.2).wait;
			~harm.set(\gate,1,\inputBus,20);
			(duration*0.2).wait;
			~fineGrain.set(\gate,1,\inbus,16,\ps, 1.0.rand);
			(duration*0.6).wait;
			"here I am".postln;0.01.wait;

			~input1.set(\inputLevel, 1.0);0.01.wait;
			~fineGrain.set(\gate,0,\inbus,14);(duration*0.2).wait;
			~harm.set(\gate,0,\inputBus,12);(duration*0.2).wait;
			~normal1.set(\gate,1,\amp, 0.3);0.01.wait;
			~normal2.set(\gate,1,\amp, 0.3);0.01.wait;
			~organ.set(\gate,1,\tr, 0.2,\pitchDev,1.0.rand,\amp,0.4, \timeDev,1.0.rand);0.01.wait;
			~base.set(\gate,1);3.0.wait;
			OSCdef(\organ).enable;0.01.wait;
			// ~remoteAddr.sendMsg(\remoteSend, 10);
		}).play;
	};

},'/tr');


OSCdef(\receiver,{|msg, time, addr|
	if ((msg[1]=='hhr') && (msg[2]==1)){
		// var counter=0;
		counter = counter+1;
		// oldTime = curTime - oldTime;
		// newTime = oldTime - time.asInteger;
	("counter = " + counter).postln;

	};
		if ((msg[1]=='hhr') && (msg[2]==1) && (counter > 4)){

			("++Hanns mutes me "+ msg[2]).postln;
			~spat.set(\amp,0);
			~base.set(\amp,0);
		counter=0;
	};
	if ((msg[1] == 'poz') && (msg[2] ==1) && (counter ==3 )){
		"++Poz sent me 1".postln;
		~spat.set(\speed,1.0.rand);
	};
	if (msg[1]=='hhr' && (msg[2]==0)){
		("++Hanns unmutes me " + msg[2]).postln;
		~spat.set(\amp,~gAmp.value);
		~base.set(\amp,0.08);
	};

},'/active');

OSCdef(\receiver).enable;
s.queryAllNodes
// [oldTime, curTime].postln;
